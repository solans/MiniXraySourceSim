#!/bin/bash

export ITK_RELEASE=itk-8.2.0.0
export ITK_PATH=$(readlink -f $(dirname ${BASH_SOURCE[0]}))
export ITK_INST_PATH=${ITK_PATH}/installed

#setup TDAQ
export TDAQ_VERSION=tdaq-08-02-00

TDAQPATHS=(/sw/atlas /cvmfs/atlas.cern.ch/repo/sw/tdaq /afs/cern.ch/atlas/project/tdaq/inst )
for _cand in ${TDAQPATHS[@]}; do
    if [ -d ${_cand}/tdaq/${TDAQ_VERSION}/installed ]; then
	export TDAQ_RELEASE_BASE=${_cand}
	export CMAKE_PROJECT_PATH=${_cand}
        break
    fi
done
LCGPATHS=(/sw/atlas/sw/lcg/releases /cvmfs/sft.cern.ch/lcg/releases /afs/cern.ch/sw/lcg/releases )
for _cand in ${LCGPATHS[@]}; do
    if [ -d ${_cand} ]; then
	export LCG_RELEASE_BASE=${_cand}
	break
    fi
done
if [ -z ${TDAQ_RELEASE_BASE+x} ]; then echo "TDAQ release base not found"; return 0; fi

echo "---------"
echo "TDAQ_RELEASE_BASE=${TDAQ_RELEASE_BASE}"
echo "LCG_RELEASE_BASE=${LCG_RELEASE_BASE}"
echo "CMAKE_PROJECT_PATH=${CMAKE_PROJECT_PATH}"
source $TDAQ_RELEASE_BASE/tdaq/${TDAQ_VERSION}/installed/setup.sh 
source $TDAQC_INST_PATH/share/cmake_tdaq/bin/setup.sh 
echo "---------"

#setup ITK SW
export PATH=$ITK_INST_PATH/$CMTCONFIG/bin:$ITK_INST_PATH/share/bin:$PATH
export LD_LIBRARY_PATH=$ITK_INST_PATH/$CMTCONFIG/lib:$LD_LIBRARY_PATH

#setup Python
export PATH=$TDAQ_PYTHON_HOME/bin:$PATH
export PYTHONPATH=$ITK_INST_PATH/share/lib/python:$PYTHONPATH
export PYTHONPATH=$ITK_INST_PATH/$CMTCONFIG/lib:$PYTHONPATH
export PYTHONPATH=$ITK_INST_PATH/share/bin:$PYTHONPATH

#setup Root
export PATH=$ROOTSYS/bin:$PATH
export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH

#missing libraries
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/pango/1.40.13/${CMTCONFIG}/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/harfbuzz/1.6.3/${CMTCONFIG}/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/cairo/1.15.8/${CMTCONFIG}/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/hdf5/1.8.18/${CMTCONFIG}/lib:$LD_LIBRARY_PATH

#setup GDB
export PYTHONHOME=$(dirname $(dirname $(which python)))
export PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/gdb/8.2.1/${CMTCONFIG}/bin:$PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/gdb/8.2.1/${CMTCONFIG}/lib:$LD_LIBRARY_PATH

#setup Geant4
source /cvmfs/geant4.cern.ch/geant4/10.5/$CMTCONFIG/bin/geant4.sh
