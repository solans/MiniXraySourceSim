#!/usr/bin/env python

import ROOT

c1 = ROOT.TCanvas("c1","c1",800,600)
box = ROOT.TGeoBBox("Box",20,30,40)
cyl = ROOT.TGeoTube("Cylinder",0,50,50)

box.Draw()
cyl.Draw("SAME")
#geo = ROOT.TGeoIntersection(box, cyl); 
#geo.Draw()

c1.Update()

raw_input()
