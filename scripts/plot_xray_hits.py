#!/usr/bin/env python
##################################################
#
# Xray simulation plots
#
# Carlos.Solans@cern.ch
# April 2017
#
##################################################

import os
import sys
import ROOT
import array
import time
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('files',nargs='+',help='config-file,root-files,bmp-files')
parser.add_argument('-o','--outdir',help='output directory',default=".")
parser.add_argument('-b','--batch',help='enable batch mode',action="store_true")
args = parser.parse_args()

class Tuple(dict):
    def __init__(self):
        self.treenumber=-1
    def LoadBranches(self,t):
        if self.treenumber == t.GetTreeNumber(): return
        self.treenumber = t.GetTreeNumber()
        for i in range(t.GetListOfLeaves().GetEntries()):
            self.__setattr__(t.GetListOfLeaves().At(i).GetName(),
                             t.GetListOfLeaves().At(i))
            pass
        pass

ROOT.gStyle.SetPadLeftMargin(0.134)
ROOT.gStyle.SetPadBottomMargin(0.146)
ROOT.gStyle.SetPadRightMargin(0.035)
ROOT.gStyle.SetPadTopMargin(0.078)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gStyle.SetLabelSize(0.05,"x")
ROOT.gStyle.SetLabelSize(0.05,"y")
ROOT.gStyle.SetTitleSize(0.05,"t")
ROOT.gStyle.SetTitleSize(0.05,"x")
ROOT.gStyle.SetTitleSize(0.05,"y")
ROOT.gStyle.SetTitleOffset(1.40,"y")
#ROOT.gStyle.SetOptTitle(0);
ROOT.gStyle.SetOptStat(0);

print "Create the Chain of events"
chain = ROOT.TChain("Hits")

print "Parse input files"
for f in args.files:
    ext=os.path.splitext(os.path.basename(f))[1]
    if "root" in ext:
        chain.AddFile(f)
    pass

hE = ROOT.TH1F("hE","hE",170,0,170)
hPos = {}

print "Loop over events"
tree = Tuple()
entry = 0
while True:
    chain.GetEntry(entry)
    if chain.LoadTree(entry)<0:
        break
    entry+=1
    tree.LoadBranches(chain)
    enum = tree.event.GetValue()
    pid  = tree.pid.GetValue()
    posx = tree.x.GetValue()
    posy = tree.y.GetValue()
    posz = tree.z.GetValue()
    edep = tree.eDep.GetValue()
    ekin = tree.eKin.GetValue()
    print enum,pid,posx,posy,posz,edep,ekin
    if pid==22 and abs(posz+10)<1.1:
        hE.Fill(ekin)
        pass
    pass

c1 = ROOT.TCanvas("c1","c1",800,600)
hE.Draw()
c1.Update()

raw_input("press any key")

print "Have a nice day"
