#!/usr/bin/env python

import os
import sys
import ROOT
import argparse

parser=argparse.ArgumentParser()
parser.add_argument('-f' ,'--files',help='output file',nargs="*",default="plots.root")
parser.add_argument('-l' ,'--lower',help='lower energy cut. default 2 keV',type=float,default=2)
parser.add_argument('-n' ,'--normalize',help='apply noise cut for florescence setup',action="store_true")
args=parser.parse_args()


class Tuple(dict):
    def __init__(self):
        self.treenumber=-1
    def LoadBranches(self,t):
        if self.treenumber == t.GetTreeNumber(): return
        self.treenumber = t.GetTreeNumber()
        for i in range(t.GetListOfLeaves().GetEntries()):
            self.__setattr__(t.GetListOfLeaves().At(i).GetName(),
                             t.GetListOfLeaves().At(i))
            pass
        pass

ROOT.gROOT.SetStyle("ATLAS")

chain = ROOT.TChain("Signals")
for fp in args.files:
    chain.AddFile(fp)
    pass

hEkin=ROOT.TH1F("hEkin",";Photon energy [keV];Number of entries",160,0,16)

tree = Tuple()
entry=0
while True:
    chain.GetEntry(entry)
    if chain.LoadTree(entry)<0: break
    entry+=1
    if entry%100000==0: print(entry)
    tree.LoadBranches(chain)
    if tree.eKin.GetValue()<args.lower: continue
    hEkin.Fill(tree.eKin.GetValue())
    pass

if args.normalize: hEkin.Scale(1.0/hEkin.Integral())

cEkin=ROOT.TCanvas("cEkin","Ekin",800,600)
cEkin.SetLogy()
hEkin.Draw()
cEkin.Update()
cEkin.SaveAs("Ekin.pdf")
cEkin.SaveAs("Ekin.C")
