
//Dear emacs this is -*-c++-*-
/************************************************
 * DetectorConstructor
 * Geant4 detector constructor
 * Carlos.Solans@cern.ch
 * August 2017
 *
 * This is Geant4 software
 ***********************************************/
#ifndef DetectorConstructor_h
#define DetectorConstructor_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"

#include <map>
#include <vector>
#include <string>

class G4VPhysicalVolume;
class G4Material;
class G4Element;
class G4VSensitiveDetector;
class G4UniformElectricField;
class G4EqMagElectricField;
class G4ClassicalRK4;
class G4MagInt_Driver;
class G4ChordFinder;

class DetectorConstructor : public G4VUserDetectorConstruction{

public:
  
  //Constructor
  DetectorConstructor();
  
  //Destructor
  ~DetectorConstructor();
  
  //Construct the detector
  G4VPhysicalVolume* Construct();

  //construct non-shared objects
  void ConstructSDandField();

  void SetTubeLength(G4float tube_length);

  void SetTubeDiameter(G4float tube_diameter);

  void SetTargetDistance(G4float target_distance);

  //void SetTargetThickness(G4float target_thickness);

  void SetTubeVoltage(G4float tube_voltage);
  
  //following lines by Silvia

  void SetCrThickness(G4float cr_thickness);
  
  void SetCuThickness(G4float cu_thickness);

  void SetBeThickness(G4float be_thickness);

  //bool CheckGeometry(m_tube_length, m_kv, m_target_distance, m_cr_thickness, m_cu_thickness, m_be_thickness, m_det_distanc);

private:
  
  float m_kv;
  float m_target_distance;
  float m_target_length;
  //float m_target_thickness;
  float m_tube_length;
  float m_tube_diameter;
  float m_sh_thickness;
  float m_cr_thickness;
  float m_cu_thickness;
  float m_be_thickness;
  float m_det_distance;
  float m_det_diameter;
  float m_det_thickness;

  std::map<std::string,G4Material*> m_mat;
  std::map<std::string,G4Element*>  m_ele;
  std::vector<G4LogicalVolume*> m_active;
  std::vector<G4LogicalVolume*> m_fields;
  std::vector<G4VSensitiveDetector*> m_sensing; 

  G4UniformElectricField * m_electricfield;
  G4EqMagElectricField * m_eqmotion;
  G4ClassicalRK4 * m_stepper;    
  G4MagInt_Driver * m_intdriver;
  G4ChordFinder * m_chordfinder;
  
};

#endif

