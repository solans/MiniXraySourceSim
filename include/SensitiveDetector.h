//Dear emacs this is -*-c++-*-
/************************************************
 * SensitiveDetector
 * Geant4 sensitive detector
 * Carlos.Solans@cern.ch
 * April 2014
 *
 * This is Geant4 software
 ***********************************************/
#ifndef SensitiveDetector_h
#define SensitiveDetector_h 1

#include "G4VSensitiveDetector.hh"

class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

class SensitiveDetector : public G4VSensitiveDetector{

public:
  
  //Constructor
  SensitiveDetector(G4String name);
  
  //Destructor
  ~SensitiveDetector();
  
  //Start of the event
  void Initialize(G4HCofThisEvent*);

  //Process every step in the volume of the detector
  G4bool ProcessHits(G4Step*,G4TouchableHistory*);
  
  //End of the event
  void EndOfEvent(G4HCofThisEvent*);
  
private:

  G4int m_hitId;
  G4int m_sumId;
  G4double m_edep;

};

#endif
