/************************************
 * Penelope Modular Physics List
 *
 * Carlos.Solans@cern.ch
 * April 2017
 ************************************/

#ifndef Penelope_h
#define Penelope_h 1

#include "G4VModularPhysicsList.hh"

class Penelope: public G4VModularPhysicsList{
 public:
  Penelope(G4int verbose=1);
  virtual ~Penelope();
  virtual void SetCuts();
};

#endif 
