//! Dear emacs this is -*-c++-*-
/************************************************
 * X-ray PhotoCathode
 * Geant4 event generator
 * Carlos.Solans@cern.ch
 * April 2017
 *
 * This is Geant4 software
 ***********************************************/
#ifndef PhotoCathode_h
#define PhotoCathode_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"

class G4Event;
class G4ParticleGun;

class PhotoCathode : public G4VUserPrimaryGeneratorAction{
public:
  
  //Constructor
  PhotoCathode(float energy, float nparticles, float size);
  
  //Destructor
  ~PhotoCathode();
  
  //Callback method to get next event
  virtual void GeneratePrimaries(G4Event*);
  
private:

  //Particle gun
  G4ParticleGun* m_gun;  

  //PhotoCathode size
  float m_size;

};

#endif
