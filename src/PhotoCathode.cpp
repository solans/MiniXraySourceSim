/************************************************
 * X-ray Filament
 * Geant4 event generator
 * Carlos.Solans@cern.ch
 * April 2017
 *
 * This is Geant4 software
 ***********************************************/
#include "PhotoCathode.h"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

PhotoCathode::PhotoCathode(float energy, float nparticles, float size){
  //G4cout << "PhotoCathode::PhotoCathode" << G4endl; 
  m_size=size;
  m_gun = new G4ParticleGun(nparticles); 
  m_gun->SetParticleDefinition(G4ParticleTable::GetParticleTable()->FindParticle("e-"));
  m_gun->SetParticlePosition(G4ThreeVector(0.*cm,0.*cm,0.*cm));  
  m_gun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1));
  m_gun->SetParticleEnergy(energy);
}

PhotoCathode::~PhotoCathode(){
  //G4cout << "PhotoCathode::~PhotoCathode" << G4endl;
  delete m_gun;
}

void PhotoCathode::GeneratePrimaries(G4Event* event){
  //G4cout << "PhotoCathode::GeneratePrimaries" << G4endl;
  m_gun->SetParticlePosition(G4ThreeVector(0.*cm,0.*cm,0.*cm));
  m_gun->GeneratePrimaryVertex(event);
}


