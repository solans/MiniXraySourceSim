/*************************************************
 * X-ray simulation
 * Carlos.Solans@cern.ch
 * April 2017
 *
 * This is Geant 4 software
 *************************************************/

#include "globals.hh"
#include "G4Timer.hh"
#include "G4RunManager.hh"
#include "G4HepRepFile.hh"
#include "G4VisManager.hh"
#include "G4UImanager.hh"
#include "G4UIExecutive.hh"
#include "G4VisExecutive.hh"
#include "G4AnalysisManager.hh"

#include "DetectorConstructor.h"
#include "PhotoCathode.h"
#include "Penelope.h"
#include "SteppingAction.h"

#include "G4ios.hh"

#include <cmdl/cmdargs.h>
#include <signal.h>

using namespace std;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  G4RunManager* runManager = G4RunManager::GetRunManager();
  runManager->AbortRun(true);
}


int main(int argc, char** argv){
  
  //Parse arguments
  CmdArgInt nevents ('n', "nevents", "events", "number of events (1)");
  CmdArgStr ofile('o', "ofile", "file", "output file (output.root)");
  CmdArgInt verbose ('v', "verbose", "level", "verbose level (0)");
  CmdArgBool draw('D', "draw", "output HepRep files");
  CmdArgFloat wavelength('w', "wavelength", "nm", "laser wavelength in nm");
  CmdArgFloat intensity('i', "intensity", "number", "number of electrons per event");
  CmdArgFloat targetDistance('L', "target-distance", "mm", "target distance in cm, default 1.6 cm");
  CmdArgFloat kv('k', "kilovolt", "high-voltage", "electric field in kV, default 12 kV");
  CmdArgFloat tubeDiameter('d', "tube-diameter", "cm", "tube diameter in cm, default  2.06 cm");
  CmdArgFloat cr_thickness('r', "cr target thickness", "nm", "Cr target thickness in nm, default 50 nm");
  CmdArgFloat cu_thickness('u', "cu target thickness", "nm", "Cu target thickness in nm, default 250 nm");
  CmdArgFloat be_thickness('b', "be window thickness", "um", "Be window thickness in um, default 350 um"); 
  
  CmdLine cmd(*argv,
              & nevents,
              & ofile,
              & verbose,
              & targetDistance,
              & kv,
              & intensity,
              & wavelength,
              & draw,
              & tubeDiameter,
              & cr_thickness,
              & cu_thickness,
              & be_thickness,
              NULL);

  nevents = 1;
  verbose = 0;
  ofile = "output.root";
  wavelength = 550;
  intensity = 1000;
  
  CmdArgvIter argv_iter(--argc, ++argv);
  cmd.parse(argv_iter);
  
  //Random seeds
  G4Random::setTheEngine(new CLHEP::RanecuEngine);
  long seeds[2];
  time_t systime = time(NULL);
  seeds[0] = (long) systime;
  seeds[1] = (long) (systime*G4UniformRand());
  G4Random::setTheSeeds(seeds);  

  //Timing 
  G4Timer * timer = new G4Timer();
  timer->Start();

  //Create the run manager
  G4RunManager* runManager = new G4RunManager();
  runManager->SetVerboseLevel(verbose);

  //Create the analysis manager
  G4AnalysisManager* ana = G4AnalysisManager::Instance();
  ana->OpenFile(G4String(ofile));
  
  // Construct a detector 
  G4cout << "High voltage: " << (kv) << " kV" << G4endl;
  DetectorConstructor * detector = new DetectorConstructor();
  if(kv.flags()&CmdArg::GIVEN){detector->SetTubeVoltage(kv);}
  if(targetDistance.flags()&CmdArg::GIVEN){detector->SetTargetDistance(targetDistance);}
  if(tubeDiameter.flags()&CmdArg::GIVEN){detector->SetTubeDiameter(tubeDiameter);}
  if(cr_thickness.flags()&CmdArg::GIVEN){detector->SetCrThickness(cr_thickness);}
  if(cu_thickness.flags()&CmdArg::GIVEN){detector->SetCuThickness(cu_thickness);}
  if(be_thickness.flags()&CmdArg::GIVEN){detector->SetBeThickness(be_thickness);}
  
  runManager->SetUserInitialization(detector);
  
  // Intialize the physics lists
  //runManager->SetUserInitialization(new EmPhysicsList(verbose));
  runManager->SetUserInitialization(new Penelope(verbose));
  //runManager->SetUserInitialization(new EmStandard(verbose));


  // Initialize the run manager
  runManager->Initialize();

  // Photo-cathode
  float energy=CLHEP::h_Planck*CLHEP::c_light/(wavelength*CLHEP::nm);
  G4cout << "Photon energy: " << (energy/CLHEP::keV) << " keV" << G4endl;
  runManager->SetUserAction(new PhotoCathode(energy,intensity,0));
  
  // Track photons as they enter in the detector
  runManager->SetUserAction(new SteppingAction());
  
  //Produce HepRep files if needed
  if(draw){
    G4UIExecutive * ui = new G4UIExecutive(argc, argv);
    
    G4VisManager* visManager = new G4VisExecutive();
    visManager->Initialize();
    
    G4UImanager* uiManager = G4UImanager::GetUIpointer();
    //heprep
    uiManager->ApplyCommand("/vis/open G4HepRepFile");
    //uiManager->ApplyCommand("/vis/open RayTracerX");
    //uiManager->ApplyCommand("/vis/open OpenInventor");
    uiManager->ApplyCommand("/vis/viewer/create");
    //  uiManager->ApplyCommand("/vis/sceneHandler/create G4HepRepFile");
    uiManager->ApplyCommand("/vis/scene/create");
    uiManager->ApplyCommand("/vis/drawVolume");
    uiManager->ApplyCommand("/vis/scene/add/axes");
    //uiManager->ApplyCommand("/tracking/verbose 1");
    uiManager->ApplyCommand("/vis/scene/add/trajectories");
    //uiManager->ApplyCommand("/vis/modeling/trajectories/create/drawByCharge");
    //uiManager->ApplyCommand("/vis/modeling/trajectories/drawByCharge-0/default/setDrawStepPts true");
    //uiManager->ApplyCommand("/vis/modeling/trajectories/drawByCharge-0/default/setStepPtsSize 2");
    uiManager->ApplyCommand("/vis/scene/add/hits");
    uiManager->ApplyCommand("/vis/viewer/flush");
    G4cout << "uiManager where are you?" << G4endl;
    
    //detector->SetUIactive();
    ui->SessionStart();
    delete ui;
    
    //delete visualization manager
    delete visManager;
    
  }else{

    //Run the events
    G4cout << "BeamON" << G4endl;
    signal(SIGINT, handler);  
    signal(SIGTERM, handler);  
    signal(SIGKILL, handler);  
    runManager->BeamOn(nevents);
    G4cout << "BeamOFF" << G4endl;
    
  }
  
  
  ana->Write();
  ana->CloseFile();  
  delete ana;
  delete runManager;
  
  timer->Stop();
  G4cout << "Simulation time: " << timer->GetRealElapsed() << " s " << G4endl;
  G4cout << "Have a nice day" << G4endl;
  
  delete timer;
  return 0;
}
