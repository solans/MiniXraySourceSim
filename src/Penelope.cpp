/***************************************
 * Penelope Modular Physics List 
 *
 * Carlos.Solans@cern.ch
 * April 2017
 ***************************************/
#include "Penelope.h"

#include "globals.hh"
#include "G4ios.hh"
#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleTable.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"
 
#include "G4EmPenelopePhysics.hh"
#include "G4DecayPhysics.hh"
#include "G4EmStandardPhysics.hh"
#include "G4EmExtraPhysics.hh"
#include "G4IonPhysics.hh"
#include "G4StoppingPhysics.hh"
#include "G4HadronElasticPhysics.hh"
#include "G4NeutronTrackingCut.hh"
#include "G4HadronPhysicsFTFP_BERT.hh"


Penelope::Penelope(G4int verbose){
  
  this->defaultCutValue = 1.*CLHEP::nm;
  
  // EM Physics --> Penelope!
  this->RegisterPhysics( new G4EmPenelopePhysics(verbose) );
    
  // Synchroton Radiation & GN Physics
  this->RegisterPhysics( new G4EmExtraPhysics(verbose) );
  
  // Decays 
  this->RegisterPhysics( new G4DecayPhysics(verbose) );
  
  // Hadron Elastic scattering
  this->RegisterPhysics( new G4HadronElasticPhysics(verbose) );
  
  // Hadron Physics
  this->RegisterPhysics(  new G4HadronPhysicsFTFP_BERT(verbose));
  
  // Stopping Physics
  this->RegisterPhysics( new G4StoppingPhysics(verbose) );
  
  // Ion Physics
  this->RegisterPhysics( new G4IonPhysics(verbose));
  
  // Neutron tracking cut
  this->RegisterPhysics( new G4NeutronTrackingCut(verbose));

}

Penelope::~Penelope(){
}

void Penelope::SetCuts(){
  SetCutValue(defaultCutValue, "gamma");   
  SetCutValue(defaultCutValue, "e-");   
  SetCutValue(defaultCutValue, "e+");   
  SetCutValue(defaultCutValue, "proton");   
}
