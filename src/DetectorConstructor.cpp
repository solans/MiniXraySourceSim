/************************************************
 * DetectorConstructor.cpp
 * Geant4 detector constructor
 * author: Carlos.Solans@cern.ch
 * date: August 2014
 *
 * This is based on Geant4 software
 ***********************************************/
#include "DetectorConstructor.h"
#include "SensitiveDetector.h"

#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4Element.hh"
#include "G4ElementTable.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4PVParameterised.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4GDMLParser.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"
#include "G4UniformElectricField.hh"
#include "G4EqMagElectricField.hh"
#include "G4ClassicalRK4.hh"
#include "G4MagIntegratorDriver.hh"
#include "G4ChordFinder.hh"
#include "G4NistManager.hh"
#include "G4AnalysisManager.hh"
#include <sstream>
#include <vector>

DetectorConstructor::DetectorConstructor()
{
  G4cout << "DetectorConstructor::DetectorConstructor" << G4endl;
  m_ele["Be"] = new G4Element("Beryllium", "Be", 4., 9.01*g/mole);
  m_ele["N"]  = new G4Element("Nitrogen","N",  7.,  14.01*g/mole);
  m_ele["O"]  = new G4Element("Oxygen", "O",   8.,  16.00*g/mole);
  m_ele["Al"] = new G4Element("Al-27", "Al",  13.,  26.98*g/mole);
  m_ele["Si"] = new G4Element("Silicon","Si", 14.,  28.09*g/mole);
  m_ele["Cr"] = new G4Element("Chromium", "Cr", 24.,51.99*g/mole);
  m_ele["Cu"] = new G4Element("Copper", "Cu", 29.,  62.93*g/mole);
  m_ele["W"]  = new G4Element("Tungsten","W", 74., 183.85*g/mole);
  m_ele["Pb"] = new G4Element("Pb-208","Pb",  82., 207.00*g/mole);

  m_mat["Galactic"] = new G4Material("Galactic", 1., 1.01*g/mole, CLHEP::universe_mean_density, 
                                     kStateGas,0.1*kelvin,1.e-19*pascal);
  m_mat["SiO2"] = new G4Material("Silicon oxide", 2.65*g/cm3, 2);
  m_mat["SiO2"] ->AddElement(m_ele["Si"], 1);
  m_mat["SiO2"] ->AddElement(m_ele["O"],  2);
  m_mat["W"]    = new G4Material("Tungsten", 19.25*g/cm3, 1);
  m_mat["W"]    ->AddElement(m_ele["W"], 1);
  m_mat["Pb"]   = new G4Material("Lead", 11.34*g/cm3, 1);
  m_mat["Pb"]   ->AddElement(m_ele["Pb"], 1);
  m_mat["Al"]   = new G4Material("Aluminum", 2.70*g/cm3, 1);
  m_mat["Al"]   ->AddElement(m_ele["Al"], 1);
  m_mat["Cu"]   = new G4Material("Copper", 8.96*g/cm3, 1);
  m_mat["Cu"]   ->AddElement(m_ele["Cu"], 1);
  m_mat["Cr"]   = new G4Material("Chromium", 7.19*g/cm3, 1);
  m_mat["Cr"]   ->AddElement(m_ele["Cr"], 1);
  m_mat["Be"]   = new G4Material("Beryllium", 1.85*g/cm3, 1);
  m_mat["Be"]   ->AddElement(m_ele["Be"], 1);
  m_mat["Air"]  = new G4Material("Air", 1.290*mg/cm3,2);
  m_mat["Air"]  ->AddElement(m_ele["N"], 70.0*perCent); 
  m_mat["Air"]  ->AddElement(m_ele["O"], 30.0*perCent);

  m_tube_length=2.3*CLHEP::cm;
  m_tube_diameter=2.06*CLHEP::cm;
  m_kv=12.*CLHEP::kilovolt;
  m_target_distance=1.6*CLHEP::cm;
  m_sh_thickness=1.0*CLHEP::cm;
  m_cr_thickness=50.*CLHEP::nm;
  m_cu_thickness=250.*CLHEP::nm;
  m_be_thickness=350.*CLHEP::micrometer;
  m_det_distance=0.5*CLHEP::cm;
  m_det_diameter=5.0*CLHEP::cm;
  m_det_thickness=2.0*CLHEP::cm;

  
}

DetectorConstructor::~DetectorConstructor()
{
  G4cout << "DetectorConstructor::~DetectorConstructor" << G4endl;
  for(std::map<std::string,G4Material*>::iterator it=m_mat.begin(); it!=m_mat.end(); ++it){
    delete it->second;
  }
  m_mat.clear();
  for(std::map<std::string,G4Element*>::iterator it=m_ele.begin(); it!=m_ele.end(); ++it){
    delete it->second;
  }
  m_ele.clear();  
}

void DetectorConstructor::SetTubeLength(G4float tube_length){
  m_tube_length=tube_length*CLHEP::cm;
}

void DetectorConstructor::SetTubeDiameter(G4float tube_diameter){
  m_tube_diameter=tube_diameter*CLHEP::cm;
}

void DetectorConstructor::SetTargetDistance(G4float target_distance){
  m_target_distance=target_distance*CLHEP::cm;
}

void DetectorConstructor::SetTubeVoltage(G4float tube_voltage){
  m_kv=tube_voltage*CLHEP::kilovolt;
}

void DetectorConstructor::SetCrThickness(G4float cr_thickness){
  m_cr_thickness=cr_thickness*CLHEP::nm;
}

void DetectorConstructor::SetCuThickness(G4float cu_thickness){
  m_cu_thickness=cu_thickness*CLHEP::nm;
}

void DetectorConstructor::SetBeThickness(G4float be_thickness){
  m_be_thickness=be_thickness*CLHEP::um;
}

G4VPhysicalVolume* DetectorConstructor::Construct()
{
  G4cout << "DetectorConstructor::Construct" << G4endl;

  // Store config into output file

  G4AnalysisManager* man = G4AnalysisManager::Instance();
  G4int id=man->CreateNtuple("Metadata", "Metadata");
  G4cout << "DetectorConstructor Ntuple id: " << id << G4endl;
  man->CreateNtupleIColumn("tubeLength");
  man->CreateNtupleIColumn("tubeDiameter");
  man->CreateNtupleDColumn("EFieldVoltage");
  man->CreateNtupleDColumn("targetDistance");
  man->CreateNtupleDColumn("CrThickness");
  man->CreateNtupleDColumn("CuThickness");
  man->CreateNtupleDColumn("BeThickness");
  man->CreateNtupleDColumn("DetectorDistance");
  man->CreateNtupleDColumn("DetectorDiameter");
  man->CreateNtupleDColumn("DetectorThickness");
  man->FinishNtuple();

  man->FillNtupleIColumn(0,m_tube_length/CLHEP::cm); 
  man->FillNtupleIColumn(1,m_tube_diameter/CLHEP::cm); 
  man->FillNtupleDColumn(2,m_kv/CLHEP::kilovolt); 
  man->FillNtupleDColumn(3,m_target_distance/CLHEP::cm); 
  man->FillNtupleDColumn(4,m_cr_thickness/CLHEP::nm);
  man->FillNtupleDColumn(5,m_cu_thickness/CLHEP::nm);
  man->FillNtupleDColumn(6,m_be_thickness/CLHEP::micrometer);
  man->FillNtupleDColumn(7,m_det_distance/CLHEP::cm);
  man->FillNtupleDColumn(8,m_det_diameter/CLHEP::cm);
  man->FillNtupleDColumn(9,m_det_thickness/CLHEP::cm);
  man->AddNtupleRow();
 


  // Irradiation chamber
  G4Box * world_box = new G4Box("world_box",10.*cm,10.*cm,10.*m);
  G4LogicalVolume * world_vol = new G4LogicalVolume(world_box,m_mat["Galactic"],"world_vol",0,0,0);  
  world_vol->SetVisAttributes(G4VisAttributes::GetInvisible());
  G4VPhysicalVolume * world_phy = new G4PVPlacement(0,G4ThreeVector(),world_vol,"world_phy",0,false,0);
    
  //Shield  
  G4Tubs * tube_box = new G4Tubs("shield_box", m_tube_diameter,m_tube_diameter+m_sh_thickness,m_tube_length/2.,0.,360.*deg);  
  G4LogicalVolume * tube_vol = new G4LogicalVolume(tube_box,m_mat["Pb"],"tube_vol");  
  new G4PVPlacement(0,
                    G4ThreeVector(0,0,m_tube_length/2.),
                    tube_vol,
                    "target_phy",
                    world_vol,
                    false,
                    0,
                    true);
  
  //Field
  G4Tubs * field_box = new G4Tubs("field_box", 0.,m_tube_diameter,m_target_distance/2.,0.,360.*deg);  
  G4LogicalVolume * field_vol = new G4LogicalVolume(field_box,m_mat["Galactic"],"field_vol");  
  field_vol->SetVisAttributes(G4Colour::Yellow());
  m_fields.push_back(field_vol);
  new G4PVPlacement(0,
                    G4ThreeVector(0,0,m_target_distance/2.),
                    field_vol,
                    "field_phy",
                    world_vol,
                    false,
                    0,
                    true);

  //Chromium target
  G4Tubs * target_cr_box = new G4Tubs("target_cr_box",0,m_tube_diameter,m_cr_thickness/2.,0.,360*deg);
  G4LogicalVolume * target_cr_vol = new G4LogicalVolume(target_cr_box,m_mat["Cr"],"target_cr_vol");  
  target_cr_vol->SetVisAttributes(G4Colour::Gray());
  m_fields.push_back(target_cr_vol);
  new G4PVPlacement(0,
                    G4ThreeVector(0,0,m_target_distance+m_cr_thickness/2.),
                    target_cr_vol,
                    "target_cr_plv",
                    world_vol,
                    false,
                    0,
                    true);
 

  //Copper target
  G4Tubs * target_cu_box = new G4Tubs("target_cu_box",0,m_tube_diameter,m_cu_thickness/2.,0.,360*deg);
  G4LogicalVolume * target_cu_vol = new G4LogicalVolume(target_cu_box,m_mat["Cu"],"target_cu_vol");  
  target_cu_vol->SetVisAttributes(G4Colour::Brown());
  m_fields.push_back(target_cu_vol);
  new G4PVPlacement(0,
                    G4ThreeVector(0,0,m_target_distance+m_cr_thickness+m_cu_thickness/2.+1*CLHEP::nm),
                    target_cu_vol,
                    "target_cu_plv",
                    world_vol,
                    false,
                    0,
                    true);
  
  //Beryllium window
  G4Tubs * window_box = new G4Tubs("window_box",0,m_tube_diameter,m_be_thickness/2.,0.,360*deg);
  G4LogicalVolume * window_vol = new G4LogicalVolume(window_box,m_mat["Be"],"window_vol");  
  window_vol->SetVisAttributes(G4Colour::Green());
  m_fields.push_back(window_vol);
  new G4PVPlacement(0,
                    G4ThreeVector(0,0,m_target_distance+m_cr_thickness+m_cu_thickness+m_be_thickness/2.+2*CLHEP::nm),
                    window_vol,
                    "window_plv",
                    world_vol,
                    false,
                    0,
                    true);

  //Air gap
  G4Tubs * airgap_box = new G4Tubs("airgap_box",0,m_det_diameter,m_det_distance/2.,0.,360*deg);
  G4LogicalVolume * airgap_vol = new G4LogicalVolume(airgap_box,m_mat["Air"],"airgap_vol");  
  new G4PVPlacement(0,
                    G4ThreeVector(0,0,m_tube_length+m_det_distance/2.),
                    airgap_vol,
                    "airgap_plv",
                    world_vol,
                    false,
                    0,
                    true);
  
  //spectrum analyser
  G4Tubs* det_box = new G4Tubs("det_box",0,m_det_diameter,m_det_thickness/2.,0., 360*deg);
  G4LogicalVolume* det_vol = new G4LogicalVolume(det_box,m_mat["SiO2"],"det_vol");   
  det_vol->SetVisAttributes(G4Colour::Blue());
  new G4PVPlacement(0,
                    G4ThreeVector(0,0,m_tube_length+m_det_distance+m_det_thickness/2.),
                    det_vol,
                    "det_plv",
                    world_vol,
                    false,
                    0,
                    true);

  m_sensing.push_back(new SensitiveDetector("/det/sensor"));
  m_active.push_back(det_vol);
  
  // Return the top physical volume
  return world_phy;
}

void DetectorConstructor::ConstructSDandField(){
  G4cout << "DetectorConstructor::ConstructSDandField" << G4endl;
  for(unsigned int i=0; i<m_sensing.size(); i++){
    G4SDManager::GetSDMpointer()->AddNewDetector(m_sensing.at(i));
    SetSensitiveDetector(m_active.at(i),m_sensing.at(i));
  }
  
  float field=-1.*m_kv/m_target_distance;
  m_electricfield = new G4UniformElectricField(G4ThreeVector(0,0,field));
  m_eqmotion = new G4EqMagElectricField(m_electricfield); 
  m_stepper = new G4ClassicalRK4(m_eqmotion, 8);       
  m_intdriver = new G4MagInt_Driver(5*nm,m_stepper,m_stepper->GetNumberOfVariables());
  m_chordfinder = new G4ChordFinder(m_intdriver);

  //Get the global field manager and attach this field
  G4FieldManager * man = G4TransportationManager::GetTransportationManager()->GetFieldManager();  
  man->SetDetectorField(m_electricfield);
  man->SetChordFinder(m_chordfinder);
  
  /*
  G4FieldManager* localFieldMgr=new G4FieldManager(m_electricfield,m_chordfinder);
  for(unsigned int i=0; i<m_fields.size(); i++){
    m_fields.at(i)->SetFieldManager(localFieldMgr,true);
  }
  */
} 

